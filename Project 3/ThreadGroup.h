/*****************************************|
| Author: Zared Hollabaugh                |
| Course: COMP 322, Advanced Programming  |
| Date: 9 September 2014                  |
| Description: Defines the ThreadGroup    |
|		       class and it's functions.  |
|*****************************************/
#pragma once
#include "Windows.h"
#include <process.h>
#include <ctime>

class ThreadGroup 
{ 
public: 
	typedef unsigned (__stdcall *TFP)(void*); 
	ThreadGroup()
	{
		numThreads=0;
		milliseconds = 0;	 
	} 
	~ThreadGroup()
	{	 
	};
	void add(TFP, void*); 
	bool execAndWait(); 
	void clear(); 
	int getElapsedTime() const; 
	int size() const; 
private:
	int numThreads;
	HANDLE threads[15];
	int milliseconds;
};
