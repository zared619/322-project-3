#pragma once
#include<vector>
/*****************************************\
| Author: Zared Hollabaugh                |
| Course: COMP 322, Advanced Programming  |
| Date: 30 October 2014                   |
| Description: Defines the MandlebrotSet  |
|		       class and it's functions.  |
\*****************************************/

#include<complex>
#include "ThreadGroup.h"

using std::vector;
using std::complex;

class MandelbrotSet 
{ 
public: 
	int totalCounts;
	static const int DEFAULT_NITERS = 150; 
	static const int DEFAULT_RADIUS = 3; 
	static unsigned __stdcall tfcn(void*);
	MandelbrotSet(complex<double> zCorner, 
		double dx, double dy, int nx, int ny, 
		int maxIterations=DEFAULT_NITERS, int radius=DEFAULT_RADIUS); 

	int iterate(complex<double>); 
	// This is the Mandelbrot iteration function. 
	vector<vector<int>>& getCounts() { return counts; } 

	void printData();
private: 
	complex<double> Corner;
	vector<vector<int>> counts; 
	int row,column,maxIterations,radius;
	double dx,dy;
};