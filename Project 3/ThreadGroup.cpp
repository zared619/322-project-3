/*****************************************\
| Author: Zared Hollabaugh                |
| Course: COMP 322, Advanced Programming  |
| Date: 9 September 2014                  |
| Description: Defines the ThreadGroup    |
|		       class and it's functions.  |
\*****************************************/

#include "ThreadGroup.h"

void ThreadGroup::add(ThreadGroup::TFP function, void *arguments)
{
	//address for beginthreadex
	unsigned int thread_add;
	//Create a new thread in a suspended state and add it to the thread array
	threads[numThreads] = (HANDLE)_beginthreadex(NULL,NULL,function,arguments,CREATE_SUSPENDED,&thread_add);
	numThreads++;
}

bool ThreadGroup::execAndWait()
{
	//check to make sure there are added threads
	//else return false
	if(numThreads>0)
	{
		//Begin timing
		clock_t begin = clock();

		//Resume all threads in the thread array
		for(int i=0; i<numThreads;i++)
		{
			ResumeThread(threads[i]);
		}
		//Wait for all threads to execute
		WaitForMultipleObjects(numThreads, threads,TRUE, INFINITE);

		//End timing
		milliseconds = clock()-begin;
		return true;
	}
	else
		return false;
}

int ThreadGroup::size() const
{
	return numThreads;
}

int ThreadGroup::getElapsedTime()const
{
	return milliseconds;
}

void ThreadGroup::clear()
{
	//Return system resources
	for(int i=0; i<numThreads; i++)
	{
		CloseHandle(threads[i]);
	}
	numThreads=0;

}