/*****************************************\
| Author: Zared Hollabaugh                |
| Course: COMP 322, Advanced Programming  |
| Date: 30 October 2014                   |
| Description: This is the driver program |
|		       Uncomment code to test     |
|			   as needed.				  |
\*****************************************/

#include "MandelbrotSet.h"
#include "ThreadGroup.h"
#include <iostream>
using std::cout;
using std::complex;

int main()
{

	ThreadGroup tgroup;
	complex<double> corner(-2.5,-2);
	double dx = 0.005, dy = 0.005;
	int nx = 500, ny = 400;
	int counts = 0,numThreads = 0;
	double time;

	//Uncomment to run earlier examples
	//corner= complex<double>(0,0);
	//dx=0.5, dy=0.5;
	//nx=3, ny=3;
	//MandelbrotSet set5(corner,dx,dy,nx,ny);
	//set5.tfcn(&set5);
	//set5.printData();
	//cout<< std::endl;

	//corner = complex<double>(-2.5, -2.0);
	//nx =10, ny = 8;
	//MandelbrotSet set6(corner,dx,dy,nx,ny);
	//set6.tfcn(&set6);
	//set6.printData();

	//nx=1000, ny = 800;
	//MandelbrotSet set7(corner,dx,dy,nx,ny);
	//tgroup.add(MandelbrotSet::tfcn, &set7);
	//tgroup.execAndWait();
	//time = tgroup.getElapsedTime()*0.001;
	//counts+=set7.totalCounts;
	//cout<< counts << " counts computed in "<< time << " seconds!"<<std::endl;
	//counts=0, time=0;


	MandelbrotSet set1(corner,dx,dy,nx,ny);

	corner = complex<double>(-2.5+500*dx, -2.0);

	MandelbrotSet set2(corner,dx,dy,nx,ny);

	corner = complex<double>(-2.5, -2.0+400*dy);

	MandelbrotSet set3(corner,dx,dy,nx,ny);

	corner = complex<double>(-2.5+500*dx, -2.0+400*dy);

	MandelbrotSet set4(corner,dx,dy,nx,ny);

	tgroup.add(MandelbrotSet::tfcn, &set1);
	numThreads++;
	tgroup.add(MandelbrotSet::tfcn, &set2);
	numThreads++;
	tgroup.add(MandelbrotSet::tfcn, &set3);
	numThreads++;
	tgroup.add(MandelbrotSet::tfcn, &set4);
	numThreads++;

	tgroup.execAndWait();
	counts += set1.totalCounts;
	counts+=set2.totalCounts;
	counts+=set3.totalCounts;
	counts+=set4.totalCounts;
	time = tgroup.getElapsedTime()*0.001;
	cout<< counts << " counts computed in "<< time << " seconds with " << numThreads << " threads! " << std::endl;

	return 0;
}