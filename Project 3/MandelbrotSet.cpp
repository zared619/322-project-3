/*****************************************\
| Author: Zared Hollabaugh                |
| Course: COMP 322, Advanced Programming  |
| Date: 30 October 2014                   |
| Description: Defines the MandlebrotSet  |
|		       class and it's functions.  |
\*****************************************/

#include "MandelbrotSet.h"
#include <iostream>
using std::cout;

MandelbrotSet::MandelbrotSet(complex<double> zCorner, 
							 double dx, double dy, int nx, int ny, int maxIterations, int radius)
{
	MandelbrotSet::Corner = zCorner;
	MandelbrotSet::dx = dx;
	MandelbrotSet::dy = dy;
	MandelbrotSet::row = nx;
	MandelbrotSet::column = ny;
	MandelbrotSet::radius = pow(radius,2);
	MandelbrotSet::maxIterations = maxIterations;
	totalCounts=0;
	counts.resize(row,vector<int>(column,0));
}

unsigned __stdcall MandelbrotSet::tfcn(void* point)
{

	MandelbrotSet *p = static_cast<MandelbrotSet*> (point);
	for(int i = 0 ; i < p->row; i++)
	{
		for(int j = 0 ; j < p->column; j++)
		{
			p->counts[i][j] = p->iterate(complex<double>(p->Corner.real()+j*p->dx, p->Corner.imag()+i*p->dy));
			p->totalCounts++;
		}
	}
	return 0;
}

int MandelbrotSet::iterate(complex<double> c)
{
	complex<double> z(0,0);
	int count = 0;

	//Calculates the absolute value of z and compares to radius.
	while(((pow(z.real(),2)+pow(z.imag(),2))) < radius && count < maxIterations )
	{
		z = z*z + c ;
		count++;
	}
	return count;
}

void MandelbrotSet::printData()
{
	for(int i =0; i< row; i++)
	{
		for(int j = 0; j < column; j++)
			cout<<counts[i][j]<<'\t';
		cout<<std::endl;
	}
}
